from django.apps import AppConfig


class WebAccountConfig(AppConfig):
    name = 'web_account'
