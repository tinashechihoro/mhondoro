""" A collection of  string constants.

Public module variables:

whitespace -- a string containing all ASCII whitespace
ascii_lowercase -- a string containing all ASCII lowercase letters
ascii_uppercase -- a strung containing all ASCII uppercase letters
ascii_letters -- a string containing all ASCII letters
digits -- a string containing all ASCII decimals digits
hexdigits -- astring  containing  all ASCII hexdecimal digits
octdigits -- a string containing all ASCII octal digits
punctuation -- astring containing all ASCII puntuation characters
printable --a string containing all ASCII characters considered printable


"""

__all__ = ["ascii_letters", "ascii_lowercase", "ascii_uppercase", "capwords", "digits",
           "hexdigits", "octdigits", "printable", "puntuation", "whitespace",
           "Formatter", "Template"]
import _string

#Some stringd for ctype-style charaters  classification
whitespace = ' \t\n\r\v\f'
ascii_lowercase = 'abcdefghijklmnopqrstuvxyz'
ascii_uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
ascii_letters = ascii_lowercase + ascii_uppercase
digits = '0123456789'
hexdigits = digits + 'abcdef' + 'ABCDEF'
octdigits = '01234567'
puntuation = """!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"""
printable = digits + ascii_letters + puntuation + whitespace

#Functions  which aren't available as string methods

#Captalise the words on a string,eg "aBc dEf"

def capwords(s,sep=None):
    """capwords(s,[,sep]) -> string

    Split the argument into words  usng  split,capitalise each
    word using capitalise,and join the capitalised words using
    join.If the optional second arguments sep is absent or None
    runs of whitespace characters are replaced by a single space
    and leading and trailing whitespace are removed,otherwise sep
    is used ti split and the join the words.


    """
    return (sep or ' ').join(x.capitalize() for x in s.split(sep))
####################################################################################

import re as _re
from collections import ChainMap as _ChainMap
class _TemplateMetaClass(type):
    pattern = r"""
    %(delim)s(?:
    (?P<escape>%(delim)s) | # Escape sequence of two delimiters
    (?P<named>%(id)s)     | # delimiter and python indentifier
    {(?P<brace>%(id)s)}   | # delimiter and a brace indentifier
    (?<invalid>)          | # Other ill formaed delimeters exprs
    )
    
        
    """

    def __init__(cls,name,bases,dct):
        super(_TemplateMetaClass,cls).__init__(name,bases,dct)
        if 'pattern' in dct:
            pattern = cls.pattern

        else:
            pattern = _TemplateMetaClass.pattern % {
                'delim':_re.escape(cls.delimiter),
                'id': cls.idpattern,
            }
        cls.pattern = _re.compile((pattern,cls.flags|   _re.VERBOSE))



























